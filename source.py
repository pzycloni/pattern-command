import random

# класс blackJack
class BlackJack:
	def __init__(self):
		self.computer = random.randint(15,21)
		self.gamer = 0

	def add(self):
		self.gamer += random.randint(1,10)
		print('Компьютер: {}, Игрок: {}'.format(self.computer, self.gamer))
		if self.gamer > 21:
			print('Перебор!')
			return False
		elif self.gamer > self.computer:
			print('Вы выиграли!')
			return False
		else:
			return True

	def over(self):
		if self.gamer == self.computer:
			print('Ничья!')
			return False

		if self.gamer < self.computer:
			print('Вы проиграли!')
			return False

		print('Вы выиграли!')
		return False


# класс оболочки
class Command:
	def execute(self):
		pass


# команда взять еще
class BlackJackAdd(Command):
	def __init__(self, blackJack):
		self.blackJack = blackJack

	def execute(self):
		return self.blackJack.add()

# команда закончить игру
class BlackJackOver(Command):
	def __init__(self, blackJack):
		self.blackJack = blackJack

	def execute(self):
		return self.blackJack.over()

class BlackJackAddAndOver(Command):
	def __init__(self, blackJackAdd, blackJackOver):
		self.blackJack = {'add':blackJackAdd, 'over':blackJackOver}

	def execute(self):
		self.blackJack['add'].execute()
		return self.blackJack['over'].execute()



class Invoker:
	def run(self, cmd):
		return cmd.execute()


# отправитель
def main():
	blackJack = BlackJack()
	invoker = Invoker()

	commands = {'add':BlackJackAdd(blackJack), 'over':BlackJackOver(blackJack), 'addandover'}

	run = True
	while run is True:
		print('Menu')
		print('---------------')
		print('1.{}'.format('Еще!'))
		print('2.{}'.format('Сделать ход и остановиться!'))
		print('---------------')

		console = input('Ваш выбор:')
		vote = int(console)

		if vote == 1:
			run = invoker.run(commands['add'])

		if vote == 2:
			run = invoker.run(commands['addandover'])


main()
